import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import CallOutlined from "@material-ui/icons/CallOutlined";
import PlaceOutlined from "@material-ui/icons/PlaceOutlined";
import Map from "./Map";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    overflow: "hidden",
    padding: theme.spacing(0, 3),
  },
  paper: {
    maxWidth: 400,
    margin: `${theme.spacing(1)}px auto`,
    padding: theme.spacing(2),
  },
  avatar: {
    // margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const src = `مبدا: تهران، دانشگاه شریف`;
const des = `مقصد: تبریز، آبرسان`;
const description = `محتوای مرسوله شکستنی است.
وزن مرسوله ۵ کیلوگرم و ابعاد آن
۲۰ * ۲۰ * ۲۰ سانتی‌متر است.`;

export default function OrderDetail() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Grid container wrap="nowrap" spacing={2}>
          <Grid item>
            <Avatar className={classes.avatar}></Avatar>
          </Grid>
          <Grid item xs zeroMinWidth>
            <Typography noWrap>محمد محمدی</Typography>
          </Grid>
        </Grid>
      </Paper>
      <Paper className={classes.paper}>
        <Grid container wrap="nowrap" spacing={2}>
          <Grid item>
            <Avatar className={classes.avatar}>
              <CallOutlined />
            </Avatar>
          </Grid>
          <Grid item xs zeroMinWidth>
            <Typography noWrap>۰۹۱۲۳۴۵۶۷۸۹</Typography>
          </Grid>
        </Grid>
      </Paper>
      <Paper className={classes.paper}>
        <Grid container wrap="nowrap" spacing={2}>
          <Grid item>
            <Avatar className={classes.avatar}>
              <PlaceOutlined></PlaceOutlined>
            </Avatar>
          </Grid>
          <Grid item xs>
            <Typography>{src}</Typography>
          </Grid>
        </Grid>
      </Paper>
      <Paper className={classes.paper}>
        <Grid container wrap="nowrap" spacing={2}>
          <Grid item>
            <Avatar className={classes.avatar}>
              <PlaceOutlined></PlaceOutlined>
            </Avatar>
          </Grid>
          <Grid item xs>
            <Typography>{des}</Typography>
          </Grid>
        </Grid>
      </Paper>
      <Paper className={classes.paper}>
        <Grid container wrap="nowrap" spacing={2}>
          <Grid item>
            <Avatar className={classes.avatar}>
              <PlaceOutlined></PlaceOutlined>
            </Avatar>
          </Grid>
          <Grid item xs>
            <Typography>{description}</Typography>
          </Grid>
        </Grid>
      </Paper>
      <Paper className={classes.paper}>
        <Grid container wrap="nowrap" spacing={2}>
          <Grid item xs>
            <Map></Map>
          </Grid>
        </Grid>
      </Paper>
      <Button
        type="submit"
        fullWidth
        variant="contained"
        color="primary"
        className={classes.submit}
      >
        چاپ
      </Button>
    </div>
  );
}
