import React, { Component } from "react";
import GoogleMapReact from "google-map-react";
import { Avatar } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import LocalShippingOutlined from "@material-ui/icons/LocalShippingOutlined";

const useStyles = makeStyles((theme) => ({
  avatar: {
    backgroundColor: theme.palette.secondary.main,
  },
}));

const AnyReactComponent = () => {
  const classes = useStyles();
  return (
    <div>
      <Avatar className={classes.avatar}>
        <LocalShippingOutlined />
      </Avatar>
    </div>
  );
};

class Map extends Component {
  static defaultProps = {
    center: {
      lat: 35.7036,
      lng: 51.3515,
    },
    zoom: 17,
  };

  render() {
    return (
      // Important! Always set the container height explicitly
      <div style={{ height: "25vh", width: "50hv" }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyBvYHPTcJQB9i98pxdcUkccT0QKaJ17TVU" }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
        >
          <AnyReactComponent lat={35.7036} lng={51.3515} />
        </GoogleMapReact>
      </div>
    );
  }
}

export default Map;
