import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import SignIn from "./SignIn";
import SignUp from "./SignUp";
import NewOrder from "./NewOrder";
import TrackOrder from "./TrackOrder";
import OrderDelivery from "./OrderDelivery";

export default function Home() {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/signin">ورود</Link>
            </li>
            <li>
              <Link to="/signup">ثبت نام</Link>
            </li>
            <li>
              <Link to="/neworder">ثبت سفارش</Link>
            </li>
            <li>
              <Link to="/trackorder">رهگیری مرسوله</Link>
            </li>
            <li>
              <Link to="/orderdelivery">دریافت مرسوله</Link>
            </li>
          </ul>
        </nav>

        <Switch>
          <Route path="/signin">
            <SignIn />
          </Route>
          <Route path="/signup">
            <SignUp />
          </Route>
          <Route path="/neworder">
            <NewOrder />
          </Route>
          <Route path="/trackorder">
            <TrackOrder />
          </Route>
          <Route path="/orderdelivery">
            <OrderDelivery />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
