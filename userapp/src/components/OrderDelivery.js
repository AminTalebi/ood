import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Button, Grid } from "@material-ui/core";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import Avatar from "@material-ui/core/Avatar";
import ShoppingBasketOutlined from "@material-ui/icons/ShoppingBasketOutlined";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Score from "./Score";

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 500,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function OrderDelivery() {
  const classes = useStyles();
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <ShoppingBasketOutlined />
        </Avatar>
        <Typography component="h1" variant="h5">
          دریافت مرسوله
        </Typography>
        <form className={classes.form} noValidate>
          <Typography>آیا از کیفیت خدمات راضی بودید؟</Typography>
          <Grid container direction="row" justify="center" alignItems="center">
            <Score></Score>
          </Grid>
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            required
            multiline
            rows={6}
            name="description"
            label="نظر"
            placeholder="لطفا نظرات خود درباره زمان و سلامت مرسوله‌ی دریافتی با ما اشتراک بگذارید"
            id="description"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            ثبت نظر
          </Button>
          <Grid container direction="row" justify="center" alignItems="center">
            با ثبت نظر تایید می‌کنم که بار را تحویل گرفته‌ام.
          </Grid>
        </form>
      </div>
    </Container>
  );
}
