import * as React from "react";
import AdminLayout from "./AdminLayout";
import Paper from "@material-ui/core/Paper";
import {withStyles} from "@material-ui/styles";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import {GoogleApiWrapper, InfoWindow, Map, Marker} from 'google-maps-react';


const styles = theme => ({
    view: {
        ...theme.mixins.gutters(),
        padding: theme.spacing.unit * 4,
    },
    fields: {
        '& > *': {
            margin: '0 48px 0 0',
        },
    },
    submit: {
        marginTop: '24px',
        textAlign: 'right',
    }
});


class MapContainer extends React.Component {
    render() {
        return (
            <Map google={this.props.google} zoom={14} containerStyle={{
                position: 'relative',
                width: '100%',
                height: '500px'
            }}>
                <Marker
                    title={'Car location.'}
                    position={this.props.position}
                    name={'Current location'}/>

                <InfoWindow>
                    <div>
                        <h1>shit</h1>
                    </div>
                </InfoWindow>
            </Map>
        );
    }
}

const MapBox = GoogleApiWrapper({
    apiKey: ('AIzaSyBvYHPTcJQB9i98pxdcUkccT0QKaJ17TVU')
})(MapContainer)

class ViewDriverLocation extends React.PureComponent {
    render() {
        return (
            <AdminLayout title={"مشاهده‌ی موقعیت راننده"} backUrl={"/admin/drivers"}>
                <Paper className={this.props.classes.view}>
                    <MapBox position={{lat: 37.778519, lng: -122.405640}}/>
                </Paper>
            </AdminLayout>
        );
    }
}

export default withStyles(styles)(ViewDriverLocation);
