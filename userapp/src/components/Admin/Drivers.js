import * as React from "react";
import AdminLayout from "./AdminLayout";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Table from "@material-ui/core/Table";
import Paper from "@material-ui/core/Paper";
import {withStyles} from "@material-ui/styles";
import TablePagination from "@material-ui/core/TablePagination";
import PreviewIcon from '@material-ui/icons/Search';
import DeleteIcon from '@material-ui/icons/Delete';
import LocationIcon from '@material-ui/icons/LocationOn';
import HistoryIcon from '@material-ui/icons/History';
import Fab from "@material-ui/core/Fab";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import {LinkedFab} from "./LinkedFab";
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';

const rows = [
    ["۱", "محمدمهدی محمدی", "۰۳۷۱۴۷۹۴۹۵", "payam.int", "فعال", "4.5"]
];

const styles = theme => ({
    driversPaper: {
        ...theme.mixins.gutters(),
        padding: theme.spacing.unit * 2,
    },
    fab: {
        margin: theme.spacing.unit,
    },
    search: {
        marginBottom: theme.spacing.unit * 4,
        // padding: theme.spacing.unit,
        display: 'flex',
        alignItems: 'baseline',
        '& > *': {
            margin: '0 12px'
        },
        '& > *:first-of-type': {
            marginRight: '0'
        }
    }
});

class Drivers extends React.PureComponent {
    render() {
        return (
            <AdminLayout title={"رانندگان"}>
                <div className={this.props.classes.search}>
                    <TextField label="شماره"/>
                    <TextField label="نام و نام‌خانوادگی"/>
                    <TextField label="نام کاربری"/>
                    <FormControl style={{width: '200px'}}>
                        <InputLabel>وضعیت</InputLabel>
                        <Select>
                            <MenuItem value={""}>درحال ثبت‌نام</MenuItem>
                            <MenuItem value={"2"}>درحال تکمیل اطلاعات</MenuItem>
                            <MenuItem value={"3"}>فعال</MenuItem>
                            <MenuItem value={"4"}>مسدود</MenuItem>
                        </Select>
                    </FormControl>
                    <Button variant={"contained"} color={"primary"}>جستجو</Button>
                </div>
                <Paper>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>#</TableCell>
                                <TableCell>نام و نام خانوادگی</TableCell>
                                <TableCell>شماره ملی</TableCell>
                                <TableCell>نام کاربری</TableCell>
                                <TableCell>وضعیت</TableCell>
                                <TableCell> <KeyboardArrowDownIcon/>امتیاز</TableCell>
                                <TableCell>اعمال</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map(row => (
                                <TableRow key={row[0]}>
                                    {row.map(cell => <TableCell>{cell}</TableCell>)}
                                    <TableCell>
                                        <LinkedFab to={'/admin/drivers/view'} size="medium" color="default"
                                                   aria-label="Add"
                                                   className={this.props.classes.fab}> <PreviewIcon/> </LinkedFab>
                                        <LinkedFab to={'/admin/drivers/delete'} size="medium" color="secondary"
                                                   aria-label="Delete"
                                                   className={this.props.classes.fab}> <DeleteIcon/> </LinkedFab>
                                        <LinkedFab to={'/admin/drivers/location'} size="medium" color="primary"
                                                   aria-label="Location"
                                                   className={this.props.classes.fab}> <LocationIcon/> </LinkedFab>
                                        <LinkedFab to={'/admin/drivers/history'} size="medium" color="primary"
                                                   aria-label="History"
                                                   className={this.props.classes.fab}> <HistoryIcon/> </LinkedFab>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>

                <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={50}
                    labelDisplayedRows={({from, to, count}) => `در حال نمایش ${from}-${to} از ${count}`}
                    labelRowsPerPage={"تعداد در صفحه"}
                    rowsPerPage={25}
                    page={1}
                    backIconButtonProps={{
                        'aria-label': 'Previous Page',
                    }}
                    nextIconButtonProps={{
                        'aria-label': 'Next Page',
                    }}
                    // onChangePage=
                    // onChangeRowsPerPage={this.handleChangeRowsPerPage}
                />
            </AdminLayout>
        );
    }
}

export default withStyles(styles)(Drivers);
