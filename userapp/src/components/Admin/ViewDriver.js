import * as React from "react";
import AdminLayout from "./AdminLayout";
import Paper from "@material-ui/core/Paper";
import {withStyles} from "@material-ui/styles";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";


const styles = theme => ({
    view: {
        ...theme.mixins.gutters(),
        padding: theme.spacing.unit * 4,
    },
    fields: {
        '& > *': {
            margin: '0 48px 0 0',
        },
    },
    submit: {
        marginTop: '24px',
        textAlign: 'right',
    }
});

class Drivers extends React.PureComponent {
    render() {
        return (
            <AdminLayout title={"مشاهده‌ی راننده"} backUrl={"/admin/drivers"}>
                <Paper className={this.props.classes.view}>
                    <div className={this.props.classes.fields}>
                        <TextField label="شماره" value={1} disabled={1}/>
                        <TextField label="نام و نام‌خانوادگی" value={'محمدمهدی محمدی'}/>
                        <TextField label="نام کاربری" value={'payam.int'}/>
                        <FormControl style={{width: '200px'}}>
                            <InputLabel>وضعیت</InputLabel>
                            <Select value={3}>
                                <MenuItem value={""}>درحال ثبت‌نام</MenuItem>
                                <MenuItem value={"2"}>درحال تکمیل اطلاعات</MenuItem>
                                <MenuItem value={3}>فعال</MenuItem>
                                <MenuItem value={"4"}>مسدود</MenuItem>
                            </Select>
                        </FormControl>
                    </div>
                    <div className={this.props.classes.submit}>
                        <Button variant={"contained"} color={"primary"}>ذخیره تغییرات</Button>
                    </div>
                </Paper>
            </AdminLayout>
        );
    }
}

export default withStyles(styles)(Drivers);
