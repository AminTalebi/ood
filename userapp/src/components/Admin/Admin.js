import Dashboard from "./Dashboard";
import {Route, Switch} from "react-router-dom";
import * as React from "react";
import {createMuiTheme} from "@material-ui/core";
import rtl from 'jss-rtl';
import {jssPreset, StylesProvider, ThemeProvider} from "@material-ui/styles";
import {create} from "jss";
import Drivers from "./Drivers";
import ViewDriver from "./ViewDriver";
import ViewDriverLocation from "./ViewDriverLocation";
import Users from "./Users";
import Orders from "./Orders";
import Vehicles from "./Vehicles";

const theme = createMuiTheme({direction: 'rtl'});
const jss = create({plugins: [...jssPreset().plugins, rtl()]});

export default class Admin extends React.PureComponent {
    render() {
        return (
            <ThemeProvider theme={theme}>
                <StylesProvider jss={jss}>
                    <Switch>
                        <Route path={'/admin/drivers'} exact component={Drivers}/>
                        <Route path={'/admin/drivers/view'} exact component={ViewDriver}/>
                        <Route path={'/admin/drivers/location'} exact component={ViewDriverLocation}/>
                        <Route path={'/admin/vehicles'} exact component={Vehicles}/>
                        <Route path={'/admin/orders'} exact component={Orders}/>
                        <Route path={'/admin/users'} exact component={Users}/>
                        <Route component={Dashboard}/>
                    </Switch>
                </StylesProvider>
            </ThemeProvider>
        );
    }
}
