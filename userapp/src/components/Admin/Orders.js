import * as React from "react";
import AdminLayout from "./AdminLayout";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Table from "@material-ui/core/Table";
import Paper from "@material-ui/core/Paper";
import {withStyles} from "@material-ui/styles";
import TablePagination from "@material-ui/core/TablePagination";
import PreviewIcon from '@material-ui/icons/Search';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import {LinkedFab} from "./LinkedFab";

const rows = [
    ["۱", "محمدمهدی محمدی", "امین طالبی", "دانشگاه تهران", "دانشگاه شریف", "کامیون", "در حال انجام", "۲۲ فروردین ۱۳۹۹ - ۱۱:۵۴:۱۳"]
];

const styles = theme => ({
    driversPaper: {
        ...theme.mixins.gutters(),
        padding: theme.spacing.unit * 2,
    },
    fab: {
        margin: theme.spacing.unit,
    },
    search: {
        marginBottom: theme.spacing.unit * 4,
        // padding: theme.spacing.unit,
        display: 'flex',
        alignItems: 'baseline',
        '& > *': {
            margin: '0 12px'
        },
        '& > *:first-of-type': {
            marginRight: '0'
        }
    }
});

class Drivers extends React.PureComponent {
    render() {
        return (
            <AdminLayout title={"سفارشات"}>
                <div className={this.props.classes.search}>
                    <TextField label="شماره سفارش"/>
                    <TextField label="شماره راننده"/>
                    <TextField label="نام کاربری سفارش دهنده"/>
                    <TextField label="نام کاربری راننده"/>
                    <FormControl style={{width: '200px'}}>
                        <InputLabel>وضعیت سفارش</InputLabel>
                        <Select>
                            <MenuItem value={""}>ثبت شده</MenuItem>
                            <MenuItem value={"2"}>تخصیص‌داده شده</MenuItem>
                            <MenuItem value={"3"}>درحال انجام</MenuItem>
                            <MenuItem value={"4"}>تحویل داده‌شده</MenuItem>
                        </Select>
                    </FormControl>
                    <Button variant={"contained"} color={"primary"}>جستجو</Button>
                </div>
                <Paper>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>#</TableCell>
                                <TableCell>راننده</TableCell>
                                <TableCell>مشتری</TableCell>
                                <TableCell>مبداء</TableCell>
                                <TableCell>مقصد</TableCell>
                                <TableCell>نوع خودرو</TableCell>
                                <TableCell>وضعیت سفارش</TableCell>
                                <TableCell>تاریخ سفارش</TableCell>
                                <TableCell>اعمال</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map(row => (
                                <TableRow key={row[0]}>
                                    {row.map(cell => <TableCell>{cell}</TableCell>)}
                                    <TableCell>
                                        <LinkedFab to={'/admin/orders/view'} size="medium" color="default"
                                                   aria-label="Add"
                                                   className={this.props.classes.fab}> <PreviewIcon/> </LinkedFab>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>

                <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={50}
                    labelDisplayedRows={({from, to, count}) => `در حال نمایش ${from}-${to} از ${count}`}
                    labelRowsPerPage={"تعداد در صفحه"}
                    rowsPerPage={25}
                    page={1}
                    backIconButtonProps={{
                        'aria-label': 'Previous Page',
                    }}
                    nextIconButtonProps={{
                        'aria-label': 'Next Page',
                    }}
                    // onChangePage=
                    // onChangeRowsPerPage={this.handleChangeRowsPerPage}
                />
            </AdminLayout>
        );
    }
}

export default withStyles(styles)(Drivers);
