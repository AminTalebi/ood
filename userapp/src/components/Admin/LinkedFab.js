import React from "react";
import {Link as RouterLink} from "react-router-dom";
import Fab from "@material-ui/core/Fab";

export function LinkedFab(props) {
    const {to, ...otherProps} = props;

    const renderLink = React.useMemo(
        () => React.forwardRef((itemProps, ref) => <RouterLink to={to} ref={ref} {...itemProps} />),
        [to],
    );

    return (
        <Fab component={renderLink} {...otherProps}>{otherProps.children}</Fab>
    );
}