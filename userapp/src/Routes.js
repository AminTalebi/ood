import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import React from "react";
import Home from "./components/Home";
import Admin from "./components/Admin/Admin";

export default class Routes extends React.PureComponent {
    render() {
        return (
            <Router>
                <Switch>
                    <Route path="/admin" component={Admin}/>
                    <Route component={Home}/>
                </Switch>
            </Router>
        );
    }
}