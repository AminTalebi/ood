import 'dart:convert';

import './resource.dart';
import '../common/api.dart';
import '../common/storage.dart';

class Order {
  final String senderFirstName;
  final String senderLastName;
  final String src;
  final String des;
  final int weight;
  final String description;
  final int id;

  Order(this.senderFirstName, this.senderLastName, this.src, this.des,
      this.weight, this.description, this.id);

  factory Order.fromJson(Map<String, dynamic> json) {
    return Order(
        json['sender__user__first_name'],
        json['sender__user__last_name'],
        json['src'],
        json['des'],
        json['weight'],
        json['description'],
        json['id']);
  }

  static Resource<List<Order>> get all {
    return Resource(
        url: ORDERS_LIST,
        parse: (response) {
          final result = json.decode(utf8.decode(response.bodyBytes));
          Iterable list = result['orders'];
          return list.map((model) => Order.fromJson(model)).toList();
        });
  }
}
