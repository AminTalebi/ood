import 'package:flutter/material.dart';

var blueColor = Color(0xFF090e42);
var pinkColor = Color(0xFFff6b80);
var firstColor = Color(0XFFFC4A1F);
var secondColor = Color(0XFFAF1055);
var primaryTextColor = Color(0XFF5F6368);
var secondaryTextColor = Color(0XFFE93B2D);
var tertiaryTextColor = Color(0XFFA7A7A7);
var logoTintColor = Color(0XFFFCE3E0);
var opacityColor = Colors.white.withOpacity(0.9);

LinearGradient appGradient = LinearGradient(colors: [firstColor, secondColor]);
