import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:loading/loading.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:http/http.dart' as http;

import './pages/signin.dart';
import './pages/landing.dart';
import './pages/map.dart';
import './common/api.dart';
import './common/storage.dart';
import './pages/seeOrder.dart';
import './pages/orderDelivery.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(new MaterialApp(
      title: 'Chita',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: Colors.blue, fontFamily: 'Oxygen'),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      home: FutureBuilder(
        future: loadWidget(),
        builder: (BuildContext context, AsyncSnapshot<Widget> widget) {
          if (!widget.hasData) {
            return Scaffold(
                backgroundColor: Colors.white,
                body: Center(
                  child: Loading(
                      indicator: BallPulseIndicator(),
                      size: 100.0,
                      color: Colors.white),
                ));
          }
          return widget.data;
        },
      ),
      routes: {
        '/landing': (context) => LandingPage(),
        '/signin': (context) => SigninPage(),
        '/map': (context) => MapPage(),
        '/seeorders': (context) => OrdersListPage(),
        '/orderdelivery': (context) => OrderDeliveryPage(),
      },
      supportedLocales: [
        Locale("fa", "IR"),
      ],
      locale: Locale("fa", "IR")));
}

Future<Widget> loadWidget() async {
  bool res = await isSignedin();
  if (res) {
    return new LandingPage();
  }
  return new SigninPage();
}


Future<bool> isSignedin() async {
  String token = await storage.read(key: "jwt-access");
  debugPrint(token);
  if (token == null) {
    return false;
  }
  String body = json.encode({
    "token": token,
  });
  http.Response res = await http.post(VERIFY_TOKEN,
      body: body, headers: {'content-type': 'application/json'});

  if (res.statusCode != 200) {
    return false;
  }
  return true;
}

