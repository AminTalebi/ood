import 'package:flutter/material.dart';
import 'package:infowindow/styleguides/colors.dart';

class OrderDeliveryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('چیتا'),
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body: Builder(
        builder: (context) => SingleChildScrollView(
          child: Center(
            child: Column(
              children: <Widget>[
                SizedBox(height: 40),
                Container(
                  height: 40.0,
                  child: RaisedButton(
                    color: blueColor,
                    splashColor: Colors.blue,
                    child: const Text(
                      'بار تحویل داده شد',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onPressed: () async {},
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
