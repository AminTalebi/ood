import 'package:flutter/material.dart';
import 'package:infowindow/styleguides/colors.dart';

class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('چیتا'),
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body: Builder(
        builder: (context) => SingleChildScrollView(
          child: Center(
            child: Column(
              children: <Widget>[
                SizedBox(height: 20.0),
                Container(
                  height: 40.0,
                  child: RaisedButton(
                    color: blueColor,
                    splashColor: Colors.blue,
                    child: const Text(
                      'مشاهده سفارشات پیشنهاد شده',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onPressed: () async {
                      Navigator.pushNamed(context, '/seeorders');
                    },
                  ),
                ),
                SizedBox(height: 20.0),
                Container(
                  height: 40.0,
                  child: RaisedButton(
                    color: blueColor,
                    splashColor: Colors.blue,
                    child: const Text(
                      'خروج',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onPressed: () async {
                      Navigator.pushNamed(context, '/signin');
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
