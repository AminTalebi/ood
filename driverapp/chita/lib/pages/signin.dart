import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

import '../styleguides/colors.dart';
import '../common/api.dart';
import '../common/storage.dart';

class SigninPage extends StatefulWidget {
  @override
  _SigninPageState createState() => new _SigninPageState();
}

class _SigninPageState extends State<SigninPage> {
  final usernameCtrl = TextEditingController();
  final passwordCtrl = TextEditingController();

  @override
  void dispose() {
    usernameCtrl.dispose();
    passwordCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('چیتا'),
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body: Builder(
        builder: (context) => SingleChildScrollView(
          reverse: true,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 50.0, 15.0, 0.0),
                      child: Text(
                        'ورود',
                        style: TextStyle(
                            fontSize: 80.0,
                            fontWeight: FontWeight.bold,
                            color: blueColor),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                  padding: EdgeInsets.only(top: 35.0, left: 20.0, right: 20.0),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            bottom: MediaQuery.of(context).viewInsets.bottom),
                        child: Directionality(
                          textDirection: TextDirection.ltr,
                          child: TextField(
                            style: TextStyle(color: Colors.blueAccent),
                            decoration: InputDecoration(
                                labelText: 'نام کاربری',
                                labelStyle: TextStyle(
                                  fontSize: 12,
                                  color: Colors.blue,
                                  fontWeight: FontWeight.bold,
                                ),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.lightBlueAccent))),
                            keyboardType: TextInputType.text,
                            controller: usernameCtrl,
                          ),
                        ),
                      ),
                      SizedBox(height: 20.0),
                      Padding(
                        padding: EdgeInsets.only(
                            bottom: MediaQuery.of(context).viewInsets.bottom),
                        child: Directionality(
                          textDirection: TextDirection.ltr,
                          child: TextField(
                            style: TextStyle(color: Colors.blue),
                            decoration: InputDecoration(
                                labelText: 'گذرواژه',
                                labelStyle: TextStyle(
                                  fontSize: 12,
                                  color: Colors.blue,
                                  fontWeight: FontWeight.bold,
                                ),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.lightBlueAccent))),
                            obscureText: true,
                            controller: passwordCtrl,
                          ),
                        ),
                      ),
                      SizedBox(height: 40.0),
                      Container(
                        height: 40.0,
                        child: RaisedButton(
                          color: blueColor,
                          splashColor: Colors.blue,
                          child: const Text(
                            'ورود',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          onPressed: () async {
                            debugPrint(usernameCtrl.text);
                            debugPrint(passwordCtrl.text);
                            if (usernameCtrl.text.length == 0 ||
                                passwordCtrl.text.length == 0) {
                              Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text(
                                    "لطفا نام کاربری و گذرواژه‌ی خود را وارد کنید"),
                              ));
                              return;
                            }
                            bool ok = await attemptSignin(
                                usernameCtrl.text, passwordCtrl.text);
                            if (ok) {
                              await storage.write(
                                  key: "username", value: usernameCtrl.text);
                              Navigator.pushReplacementNamed(
                                  context, '/landing');
                            } else {
                              Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text("ورود ناموفق"),
                              ));
                            }
                          },
                        ),
                      ),
                      SizedBox(height: 20.0),
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }
}

Future<bool> attemptSignin(String username, String password) async {
  String body = json.encode({"username": username, "password": password});

  var res = await http
      .post(SIGNIN, body: body, headers: {'content-type': 'application/json'});
  debugPrint(res.statusCode.toString());
  debugPrint(res.body.toString());
  if (res.statusCode == 200) {
    Map<String, dynamic> tokens = json.decode(res.body);
    debugPrint(tokens['access']);
    debugPrint(tokens['refresh']);
    await storage.write(key: "jwt-access", value: tokens['access']);
    await storage.write(key: "jwt-refresh", value: tokens['refresh']);
    return true;
  }
  return false;
}
