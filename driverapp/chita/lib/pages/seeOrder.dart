import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import '../models/order.dart';
import '../models/resource.dart';
import '../common/storage.dart';
import '../styleguides/colors.dart';
import '../common/api.dart';

class OrdersListPage extends StatefulWidget {
  @override
  _OrdersListState createState() {
    return new _OrdersListState();
  }
}

class _OrdersListState extends State<OrdersListPage> {
  List<Order> _orders = List<Order>();

  @override
  void initState() {
    super.initState();
    _populateOrders();
  }

  ListTile _buildItemsForListView(BuildContext context, int index) {
    return ListTile(
      leading: Icon(Icons.assignment),
      title: Text("مقصد: " + _orders[index].des),
      subtitle:
          Text("مبدا: " + _orders[index].src, style: TextStyle(fontSize: 18)),
      isThreeLine: true,
      onTap: () {},
      trailing: RaisedButton(
        color: blueColor,
        splashColor: Colors.blue,
        child: const Text(
          'قبول',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        onPressed: () async {
          String username = await storage.read(key: "username");
          String body = json.encode({
            "driver_username": username,
            "order_id": _orders[index].id,
          });
          var res = await http.post(ACCEPT_ORDER,
              body: body, headers: {'content-type': 'application/json'});

          if (res.statusCode == 200) {
            await storage.write(key: "active_order_id", value: _orders[index].id.toString());
            Navigator.pushNamed(context, '/map');
          } else {
            Scaffold.of(context).showSnackBar(SnackBar(
              content: Text("ارتباط با سرور برقرار نشد لطفا دوباره تلاش کنید"),
            ));
          }
        },
      ),
    );
  }

  void _populateOrders() async {
    String username = await storage.read(key: "username");
    debugPrint(username);
    Webservice().load(Order.all, username).then((orders) => {
          setState(() => {_orders = orders})
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('سفارشات'),
        ),
        body: ListView.builder(
          itemCount: _orders.length,
          itemBuilder: _buildItemsForListView,
        ));
  }
}
