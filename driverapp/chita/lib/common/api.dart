const SERVER_URL = 'https://ood.liara.run';
const SIGNIN = '$SERVER_URL/accounts/api/token';
const VERIFY_TOKEN = '$SERVER_URL/accounts/api/token/verify';
const CURRENT_USER = '$SERVER_URL/api/accounts/current_user';
const ORDERS_LIST = '$SERVER_URL/orders/api/suggested/';
const ACCEPT_ORDER = '$SERVER_URL/orders/api/accept';
const UPDATE_LOCATION = '$SERVER_URL/locations/';
