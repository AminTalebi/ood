package ir.sharif.ood.chitacore.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.sharif.ood.chitacore.config.RedisConfiguration;
import ir.sharif.ood.chitacore.dto.LocationUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveListOperations;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class RedisQueue implements Queue {
    private final RedisConfiguration redisConfiguration;
    private final ObjectMapper objectMapper;

    @Autowired
    public RedisQueue(RedisConfiguration redisConfiguration) {
        this.redisConfiguration = redisConfiguration;
        this.objectMapper = new ObjectMapper();
    }

    @Override
    public void push(String id, LocationUpdate locationUpdate) {
        try {
            this.redisConfiguration.getResource().lpush(id, objectMapper.writeValueAsString(locationUpdate));
        } catch (JsonProcessingException e) {
        }
    }

    @Override
    public LocationUpdate pop(String id) {
        return null;
    }

    @Override
    public List<LocationUpdate> peek(String id, int k) {
        List<String> lrange = this.redisConfiguration.getResource().lrange(id, 0, k - 1);
        return lrange.stream().map(e -> {
            try {
                return this.objectMapper.readValue(e, LocationUpdate.class);
            } catch (JsonProcessingException ex) {
                return null;
            }
        }).filter(Objects::nonNull).collect(Collectors.toList());
    }
}
