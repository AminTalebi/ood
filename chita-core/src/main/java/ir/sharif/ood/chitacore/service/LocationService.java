package ir.sharif.ood.chitacore.service;

import ir.sharif.ood.chitacore.common.Queue;
import ir.sharif.ood.chitacore.dto.LocationUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class LocationService {
    private final Queue queue;

    @Autowired
    public LocationService(Queue queue) {
        this.queue = queue;
    }

    public void putLocation(String uid, LocationUpdate locationUpdate) {
        locationUpdate.setTimestamp(new Timestamp(System.currentTimeMillis()));
        queue.push(uid, locationUpdate);
    }

    public List<LocationUpdate> getLatestInfo(String uid, Integer size) {
        return queue.peek(uid, size);
    }

}
