package ir.sharif.ood.chitacore.dto;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
public class LocationUpdate {
    @NotNull
    @Min(-90)
    @Max(+90)
    private double lat;
    @NotNull
    @Min(-180)
    @Max(+180)
    private double lon;
    private Timestamp timestamp;
}
