package ir.sharif.ood.chitacore.controller;

import ir.sharif.ood.chitacore.dto.LocationUpdate;
import ir.sharif.ood.chitacore.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/locations")
public class LocationController {
    private final LocationService locationService;

    @Autowired
    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @PostMapping("/{id}")
    public ResponseEntity pushLocation(@PathVariable String id, @RequestBody LocationUpdate locationUpdate) {
        this.locationService.putLocation(id, locationUpdate);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}/{size}")
    public ResponseEntity<List<LocationUpdate>> getLocation(@PathVariable String id, @PathVariable Integer size) {
        return ResponseEntity.ok().body(this.locationService.getLatestInfo(id, size));
    }

    @GetMapping("/{id}")
    public ResponseEntity<List<LocationUpdate>> getLocation(@PathVariable String id) {
        return ResponseEntity.ok().body(this.locationService.getLatestInfo(id, 5));
    }
}
