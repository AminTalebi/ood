package ir.sharif.ood.chitacore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChitaCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChitaCoreApplication.class, args);
    }

}
