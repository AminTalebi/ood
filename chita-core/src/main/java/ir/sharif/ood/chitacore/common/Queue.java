package ir.sharif.ood.chitacore.common;

import ir.sharif.ood.chitacore.dto.LocationUpdate;

import java.util.List;

public interface Queue {
    void push(String id, LocationUpdate locationUpdate);

    LocationUpdate pop(String id);

    List<LocationUpdate> peek(String id, int k);
}
