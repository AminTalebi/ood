from django.contrib import admin
from .models import Order, Comment


@admin.register(Order)
class VehicleAdmin(admin.ModelAdmin):
    list_filter = ['status']
    search_fields = ['driver__user__username', 'description', 'sender__user__username', 'vehicle__model']


admin.site.register(Comment)
