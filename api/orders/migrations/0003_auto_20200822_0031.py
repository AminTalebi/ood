# Generated by Django 3.1 on 2020-08-21 20:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0002_auto_20200822_0010'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(choices=[('Initial', 'Initial'), ('Suggested', 'Suggested'), ('Assigned', 'Assigned'), ('Informed', 'Informed'), ('Delivered', 'Delivered')], default='Initial', max_length=10),
        ),
    ]
