from django.db import models
from accounts.models import Driver, Owner
from vehicles.models import Vehicle


class Order(models.Model):
    destination_lat = models.FloatField(default=35.715298)
    destination_lon = models.FloatField(default=51.404343)
    source_lat = models.FloatField(default=35.715298)
    source_lon = models.FloatField(default=51.404343)
    lat = models.FloatField(default=35.715298)
    lon = models.FloatField(default=51.404343)
    weight = models.IntegerField()
    description = models.CharField(max_length=250)
    order_status_choices = [
        ("Initial", "Initial"),
        ("Suggested", "Suggested"),
        ("Assigned", "Assigned"),
        ("Informed", "Informed"),
        ("Delivered", "Delivered"),
    ]
    status = models.CharField(max_length=10, choices=order_status_choices, default=order_status_choices[0][0])
    driver = models.ForeignKey(to=Driver, on_delete=models.DO_NOTHING, blank=True, null=True)
    sender = models.ForeignKey(to=Owner, on_delete=models.DO_NOTHING, blank=True)

    delivery_code = models.IntegerField(null=False, blank=False)
    vehicle = models.ForeignKey(to=Vehicle, on_delete=models.DO_NOTHING, blank=True, null=True)

    def __str__(self):
        return f"{self.status} -- driver:{self.driver}, vehicle:{self.vehicle}, sender:{self.sender}"


class Comment(models.Model):
    text = models.TextField(max_length=300)
    rating_choices = [
        (1, "terrible"),
        (2, "bad"),
        (3, "fair"),
        (4, "good"),
        (5, "excellent")
    ]
    rating = models.IntegerField(default=None, blank=True, null=True, choices=rating_choices)
    order = models.ForeignKey(to=Order, on_delete=models.DO_NOTHING)
    driver = models.ForeignKey(to=Driver, blank=True, null=True, on_delete=models.DO_NOTHING)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(Comment, self).save(force_insert, force_update, using, update_fields)
        self.driver.calculate_ratings()
        self.driver.save()

    def __str__(self):
        return f"rate:{self.rating}, text:{self.text[0:10]}"
