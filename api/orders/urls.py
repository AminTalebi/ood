from django.urls import path
from .views import *
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    # path('api/suggested', SuggestedOrder.as_view(), name='suggest-order'),
    path('api/suggested/<str:driver_username>', SuggestedOrder.as_view(), name='suggest-order'),
    path('api/accept', AcceptOrder.as_view(), name='accept-order'),
    path('<int:pk>/inform_delivery', OrderInformDelivery.as_view(), name='inform-delivery'),
    path('<int:pk>/confirm_delivery', OrderConfirmDelivery.as_view(), name='confirm-delivery'),
    path('<int:pk>/comment', LeaveComment.as_view(), name='confirm-delivery'),
    path('<int:pk>', OrderDetailView.as_view(), name='order-detail'),

]
