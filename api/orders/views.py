from django.http import HttpRequest
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse
from django.views import View
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, CreateView

from accounts.models import Driver
from locations.service import *
from django.http import JsonResponse
import json
from .models import Order, Comment
from .forms import CommentForm


class SuggestedOrder(View, LoginRequiredMixin):

    def get(self, request: HttpRequest, driver_username):
        print(request.user)
        driver = Driver.objects.filter(user__username=driver_username)
        if not driver.count():
            return HttpResponse(f"no driver with username:{driver_username}")
        driver = driver[0]
        orders = driver.order_set.filter(status="Suggested").values(
            'sender__user__first_name',
            'sender__user__last_name',
            'destination_lat',
            'destination_lon',
            'source_lat',
            'source_lon',
            'description',
            'weight',
            'id'
        )
        response = []
        for order in orders:
            response.append({
                "id": order["id"],
                "sender__user__first_name": order["sender__user__first_name"],
                "sender__user__last_name": order["sender__user__last_name"],
                "description": order["description"],
                "weight": order["weight"],
                "src": LocationsService.reverse_geo(str(order['source_lat']), str(order['source_lon'])),
                "des": LocationsService.reverse_geo(str(order['destination_lat']), str(order['destination_lon']))
            })

        return JsonResponse({
            "orders": response
        }, safe=False, json_dumps_params={'ensure_ascii': False})


@method_decorator(csrf_exempt, name='dispatch')  # for csrf error...
class AcceptOrder(View):

    def post(self, request: HttpRequest):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        driver_username = body['driver_username']
        driver = Driver.objects.filter(user__username=driver_username)[0]
        order_id = body['order_id']
        suggested_order = driver.order_set.filter(pk=order_id).filter(status="Suggested")

        if suggested_order.count():
            suggested_order = suggested_order[0]
            suggested_order.status = "Assigned"
            suggested_order.save()
            self.delete_other_suggestions(driver)
            return HttpResponse(f"success!! order_id:{order_id} assigned to driver:{driver}")

        return HttpResponse(f"Error!! there is no suggested order for driver:{driver}, order_id:{order_id}")

    def delete_other_suggestions(self, driver):
        driver.order_set.filter(status='Suggested').delete()


class OrderDetailView(LoginRequiredMixin, DetailView):
    template_name = 'orders/order_info.html'
    model = Order

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        order = data['order']
        data['location'] = LocationsService.reverse_geo(str(order.lat), str(order.lon))
        return data


@method_decorator(csrf_exempt, name='dispatch')  # for csrf error...
class OrderInformDelivery(View):

    def post(self, request: HttpRequest, pk):
        order = Order.objects.filter(pk=pk)
        if not order:
            return HttpResponse("the requested order doesn't exist, please check your order_id")

        order = order[0]
        pre_status = order.status
        order.status = "Informed"
        order.save()
        return HttpResponse(f"success order: |{order}| with status:{pre_status} has been updated to:{order.status}")


class OrderConfirmDelivery(View):

    def post(self, request: HttpRequest, pk, *args, **kwargs):
        order = Order.objects.filter(pk=pk)
        if not order:
            return HttpResponse("the requested order doesn't exist, please check your order_id")
        order = order[0]
        pre_status = order.status
        order.status = "Delivered"
        order.save()
        return HttpResponse(f"success order: |{order}| with status:{pre_status} has been updated to:{order.status} "
                            f"<input type=\"button\" value=\"back\" onclick=\"history.back()\"/> ")


class LeaveComment(CreateView):
    model = Comment
    form_class = CommentForm
    template_name = 'orders/write_comment.html'

    def form_valid(self, form):
        comment = form.save(commit=False)
        order_id = self.kwargs['pk']
        order = Order.objects.filter(pk=order_id)[0]
        driver = order.driver
        comment.order = order
        comment.driver = driver
        comment.save()
        return_url = reverse('order-detail', kwargs={'pk': order_id})
        # return render(self.request, 'accounts/driver_signup.html')
        return HttpResponse(f"success comment: |{comment}| has been created "
                            f"<a href='{return_url}'>back</a>")
