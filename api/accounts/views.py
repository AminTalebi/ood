from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import CreateView, DetailView, FormView, RedirectView
from django.contrib.auth import login, logout
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.mixins import LoginRequiredMixin

from accounts.forms import OwnerSignUpForm, DriverSignUpForm
from accounts.models import User, Driver, Owner
from orders.models import Order


class OwnerSignUpView(CreateView):
    model = User
    form_class = OwnerSignUpForm
    template_name = 'accounts/owner_signup.html'

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        
        return HttpResponseRedirect(reverse('owner-profile', kwargs={'pk': user.owner.pk}))
        # return render(self.request, 'accounts/owner_signup.html')


class DriverSignUpView(CreateView):
    model = User
    form_class = DriverSignUpForm
    template_name = 'accounts/driver_signup.html'

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return render(self.request, 'accounts/driver_signup.html')


class LoginView(FormView):
    success_url = '/'
    form_class = AuthenticationForm
    template_name = 'accounts/login.html'

    def form_valid(self, form):
        login(self.request, form.get_user())

        return super(LoginView, self).form_valid(form)


class LogoutView(LoginRequiredMixin, RedirectView):
    url = '/'

    def get(self, request, *args, **kwargs):
        logout(self.request)
        return super(LogoutView, self).get(request, *args, **kwargs)


class OwnerDetailView(LoginRequiredMixin, DetailView):
    template_name = 'accounts/owner_profile.html'
    model = Owner

    def get_context_data(self, **kwargs):
        context = super(OwnerDetailView, self).get_context_data(**kwargs)
        owner = context['owner']
        # sender = Sender()
        # print(context, sender.pk, sender.user.pk)
        context['orders'] = owner.order_set.all()
        return context


class DriverDetailView(LoginRequiredMixin, DetailView):
    template_name = 'accounts/driver_profile.html'
    model = Driver

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['is_verified'] = self.get_object().is_verified
        return data
