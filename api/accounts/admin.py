from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Driver, Owner, User


# sort drivers by their rating in django admin panel
@admin.register(Driver)
class DriverAdmin(admin.ModelAdmin):
    ordering = ['-rating']
    search_fields = ['user__username']
    list_filter = ['is_verified']


class CustomUserAdmin(UserAdmin):
    fieldsets = (
        *UserAdmin.fieldsets,  # original form fieldsets, expanded
        (                      # new fieldset added on to the bottom
            'Custom Field Heading',  # group heading of your choice; set to None for a blank space instead of a header
            {
                'fields': (
                    'phone_number',
                    'picture'
                ),
            },
        ),
    )


admin.site.register(Owner)
admin.site.register(User, CustomUserAdmin)
