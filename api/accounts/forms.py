from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.core.validators import RegexValidator

from .models import Driver, Owner, User


class OwnerSignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True)
    last_name = forms.CharField(max_length=30, required=True)
    phone_regex = RegexValidator(regex=r'^\d{8,15}$',
                                 message="Please enter your phone number correctly!")
    phone_number = forms.CharField(validators=[phone_regex], required=True)
    picture = forms.ImageField(required=False)

    def save(self, commit=True):
        user = super().save(commit=False)
        if commit:
            user.save()
            owner = Owner(
                user=user,
            )
            owner.save()
        return user

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('picture', 'username', 'first_name', 'last_name', 'phone_number', 'password1', 'password2')


class DriverSignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True)
    last_name = forms.CharField(max_length=30, required=True)
    phone_regex = RegexValidator(regex=r'^\d{8,15}$',
                                 message="Please enter your phone number correctly!")
    phone_number = forms.CharField(validators=[phone_regex], required=True)
    picture = forms.ImageField(required=False)
    driving_license = forms.ImageField(required=True)

    def save(self, commit=True):
        user = super().save(commit=False)
        if commit:
            user.save()
            driver = Driver(
                user=user,
                driving_license=self.cleaned_data['driving_license']
            )
            driver.save()
        return user

    class Meta(UserCreationForm.Meta):
        model = User
        fields = (
            'picture', 'username', 'first_name', 'last_name', 'phone_number', 'password1', 'password2',
            'driving_license')
