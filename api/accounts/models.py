from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    phone_number = models.CharField(max_length=11, null=True, blank=True, verbose_name='Mobile number‌')
    picture = models.ImageField(blank=True, null=True, upload_to='images/profiles/')

    def __str__(self):
        return self.username

    @property
    def is_complete(self):
        if self.phone_number is None:
            return False
        if not self.is_active:
            return False
        return True


class Owner(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    num_of_orders = models.IntegerField(default=0)

    def __str__(self):
        return self.user.username


class Driver(models.Model):
    # FIXME: maybe we can move this to another app
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    driving_license = models.ImageField(null=False, blank=False, upload_to='images/driving_licenses/')
    is_verified = models.BooleanField(default=False)
    rating = models.FloatField(default=0.0)

    def calculate_ratings(self):
        ratings = [comment.rating for comment in self.comment_set.all()]
        self.rating = sum(ratings)/len(ratings)
        return self.rating

    def __str__(self):
        return self.user.username
