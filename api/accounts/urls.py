from django.urls import path
from .views import *
from rest_framework_simplejwt.views import TokenObtainPairView, TokenVerifyView

urlpatterns = [
    path('owner/signup', OwnerSignUpView.as_view(), name='owner-signup'),
    path('driver/signup', DriverSignUpView.as_view(), name='driver-signup'),
    path('login', LoginView.as_view(), name='login'),
    path('logout', LogoutView.as_view(), name='logout'),
    path('owner/<int:pk>', OwnerDetailView.as_view(), name='owner-profile'),
    path('driver/<int:pk>', DriverDetailView.as_view(), name='driver-profile'),
    path('api/token', TokenObtainPairView.as_view(), name='token-obtain'),
    path('api/token/verify', TokenVerifyView.as_view(), name='token-verify'),
]
