from django.contrib import admin

from .models import Vehicle

# admin.site.register(Vehicle)


@admin.register(Vehicle)
class VehicleAdmin(admin.ModelAdmin):
    list_filter = ['status', 'ownership']
    search_fields = ['model', 'numberplate', 'color']

