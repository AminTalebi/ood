from django.db import models


class Vehicle(models.Model):
    color = models.CharField(max_length=20, blank=True)
    year = models.DateField()
    vehicle_owner_choices = [
        ("Company", "Company"),
        ("Personal", "Personal")
    ]
    ownership = models.CharField(choices=vehicle_owner_choices, max_length=10)
    numberplate = models.CharField(max_length=20)
    model = models.CharField(max_length=20)
    picture = models.ImageField(upload_to='vehicles/images/')
    vehicle_status_choices = [
        ("Free", "Free"),
        ("Busy", "Busy"),
        ("Broke", "Broke")
    ]
    status = models.CharField(max_length=5, choices=vehicle_status_choices, default=vehicle_status_choices[0][0])

    def __str__(self):
        return f"{self.model} - {self.year.year} - {self.numberplate}"

