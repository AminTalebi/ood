from django.urls import path
from .views import LocationController

urlpatterns = [
    path("<str:username>", LocationController.as_view()),
]
