from redisorm.model import Model

import json


class Location(Model):
    def __init__(self, id=None, lat=None, lon=None, time=None):
        self.id = id
        self.lat = lat
        self.lon = lon
        self.time = time

    def repr(self):
        return json.dumps({"lat": self.lat, "lon": self.lon, "time": self.time})
