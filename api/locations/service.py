from .models import Location

import time
import requests
import logging

logger = logging.getLogger(__name__)


class LocationsService:
    @staticmethod
    def update_driver_location(driver_id, lat, lon):
        l = Location(id=driver_id, lat=lat, lon=lon, time=time.time())
        logger.info('location->', l)
        try:
            l.save()
        except Exception as e:
            logger.error(e)
            print(e)

    @staticmethod
    def get_driver_location(driver_id):
        return Location(id=driver_id).peek(-1)

    @staticmethod
    def reverse_geo(lat, lon):
        url = 'https://api.neshan.org/v2/reverse?lat=' + lat + '&lng=' + lon
        headers = {'Api-Key': 'service.qdqXBthm3vdmO7FfSFv3LiSPrCXJup2VCtTeWaA0'}
        res = requests.get(url, headers=headers)
        if res.status_code == 200:
            return res.json()['formatted_address']
        return 'could not get address'
