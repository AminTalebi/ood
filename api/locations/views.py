from django.views import View
from django.http import JsonResponse, HttpResponse
from .service import LocationsService
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
import json

import logging

logger = logging.getLogger(__name__)


@method_decorator(csrf_exempt, name='dispatch')
class LocationController(View):
    def post(self, request, username):
        body = json.loads(request.body.decode("utf-8"))
        logger.info('update location body', body)
        LocationsService.update_driver_location(username, body["lat"], body["lon"])
        return HttpResponse(status=200)

    def get(self, request, username):
        locations = LocationsService.get_driver_location(username)

        return JsonResponse({
            "locations": locations
        }, safe=False, json_dumps_params={'ensure_ascii': False})
