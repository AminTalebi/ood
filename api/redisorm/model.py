import redis

from django.conf import settings
import logging
import json

logger = logging.getLogger(__name__)

connection = redis.Redis(
    host=settings.REDIS_URL, port=settings.REDIS_PORT, db=0, decode_responses=True, password=settings.REDIS_PASSWORD
)


class AbstractModel(object):
    @classmethod
    def cache_key(cls):
        """
        This generates a `key` for a new instance being added.
        """
        pass

    def save(self):
        """
        This inserts a new instance to redis.
        """
        pass

    def peek(self, k):
        """
        This returns last k instances.
        """
        pass

    def repr(self):
        pass


class Model(AbstractModel):
    def cache_key(self):
        """
        This generates a `key` for a new instance being added.
        """
        identifier = self.id
        class_name = self.__class__.__name__.lower()
        return "%s-%s" % (class_name, identifier)

    def save(self):
        """
        This inserts a new instance to redis.
        """
        key = self.cache_key()
        logger.info('key->', key)
        connection.lpush(key, self.repr())
        return self

    def peek(cls, k=10):
        """
        This returns all instances.
        """
        key = cls.cache_key()
        members = connection.lrange(key, 0, k - 1)
        ret = []
        for mem in members:
            ret.append(json.loads(mem))
        return ret
